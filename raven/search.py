#!/usr/bin/python

#
# Project Librarian: Alex Urban
#              Graduate Student
#              UW-Milwaukee Department of Physics
#              Center for Gravitation & Cosmology
#              <alexander.urban@ligo.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Module containing time- and sky- coincidence search functions.
"""
__author__ = "Alex Urban <alexander.urban@ligo.org>"


# Imports.
import sys
import numpy as np
import healpy as hp

from gracedb_events import GW, ExtTrig
from ligo.gracedb.rest import GraceDb


# Initialize instance of GraceDB server as a global variable.
gracedb = GraceDb()


#######################################
# Functions for background estimation #
#######################################

def Cacc(rho_sky):
    """ Estimator for the cumulative fraction of accidental associations with
        sky coincidence better than psky. """
    if rho_sky < 1e-50: return 1.
    else:
        x = np.log10(rho_sky)
        p = [6.43375601e+00, -3.83233594e+04, 1.35768892e+01]
        return p[0] * x**3 / (p[1] + p[2]*x**3)



#########################################################
# Functions implementing the actual coincidence search. #
#########################################################

def query(event_type, gpstime, tl, th):
    """ Query for coincident events of type event_type occurring within a window
        of [tl, th] seconds around gpstime """

    # Perform a sanity check on the time window.
    if tl >= th:
        sys.stderr.write( "ERROR: The time window [tl, th] must have tl < th." )
        sys.exit(1)

    # Perform the GraceDB query.
    start, end = gpstime + tl, gpstime + th
    arg = '%s %s .. %s' % (event_type, start, end)

    # Return list of graceids of coincident events.
    try:
        return list(gracedb.events(arg))
    except:
        sys.stderr.write( "ERROR: Problem accessing GraCEDb while calling gracedb.events()" )
        sys.exit(1)


def search(event, tl, th):
    """ Perform a search for neighbors coincident in time within
        a window [tl, th] seconds around an event """

    # Identify neighbor types with their graceid strings.
    types = {'G': 'GW', 'E': 'External trigger', 'T': 'Test'}
    groups = {'G': 'CBC Burst', 'E': 'External'}

    # Grab any and all neighboring events.
    neighbors = query(groups[event.neighbor_type], event.gpstime, tl, th)

    # If no neighbors, report a null result.
    if not neighbors:
        message = "RAVEN: No %s candidates in window [%+d, %+d] seconds" % (types[event.neighbor_type], tl, th)
        event.submit_gracedb_log(message, tagname="ext_coinc")

    # If neighbors are found, report each of them.
    else:
        for neighbor in neighbors:
            gid = neighbor['graceid']
            message1 = "RAVEN: %s candidate found: <a href='http://gracedb.ligo.org/events/" % types[event.neighbor_type]
            message1 += "%s'>%s</a> within [%+d, %+d] seconds" % (gid, gid, tl, th)
            event.submit_gracedb_log(message1, tagname="ext_coinc") # annotate trigger with news of discovery
            try:
                gracedb.writeLabel(event.graceid, 'EM_COINC')
            except:
                pass

            message2 = "RAVEN: %s event <a href='http://gracedb.ligo.org/events/" % types[event.graceid[0]]
            message2 += "%s'>%s</a> within window [%+d, %+d] seconds" % (event.graceid, event.graceid, tl, th)
            gracedb.writeLog(gid, message2, tagname="ext_coinc") # annotate neighbor with news of discovery
            try:
                gracedb.writeLabel(gid, 'EM_COINC')
            except:
                pass

    # Return search results.
    return neighbors


def calc_signif_gracedb(gw, exttrig, tl, th, incl_sky=False):
    """ Calculate the improvement in significance that is got out of the second tier
        of this hierarchical coincidence search. """

    # The combined rate of independent GRB discovery by Swift and Fermi is 0.807 per day,
    # according to Urban et al., in prep.
    gcn_rate = 0.807 / (60 * 60 * 24)

    # Is the GW candidate's FAR sensible?
    if not gw.far:
        message = "RAVEN: WARNING: This GW candidate's FAR is a NoneType object."
        gw.submit_gracedb_log(message)
        return

    # Include sky coincidence if desired.
    if incl_sky:
        nside = hp.npix2nside( len(gw.sky_map) )
        psky = (4 * np.pi)**2 * np.sum( [x * y for x, y in zip(gw.sky_map, exttrig.sky_map(nside))] ) / len(gw.sky_map)
        far = (th - tl) * gcn_rate * Cacc( psky ) * gw.far
        message = "RAVEN: Spatiotemporal coincidence with external trigger <a href='http://gracedb.ligo.org/events/"
        message += "%s'>%s</a> gives a coincident FAR = %s Hz" % (exttrig.graceid, exttrig.graceid, far)
        gw.submit_gracedb_log(message, tagname="ext_coinc")
        return

    # Otherwise, proceed with only time coincidence.
    else:
        far = (th - tl) * gcn_rate * gw.far
        message = "RAVEN: Temporal coincidence with external trigger <a href='http://gracedb.ligo.org/events/"
        message += "%s'>%s</a> gives a coincident FAR = %s Hz" % (exttrig.graceid, exttrig.graceid, far)
        gw.submit_gracedb_log(message, tagname="ext_coinc")
        return
